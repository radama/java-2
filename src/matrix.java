	
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class matrix{
public int w;
public int h;


public int[][] data;
private Scanner sc;

public matrix(int h, int w) {
	this.w = w; 
	this.h = h;
	data=new int [h][w];
	Random rand = new Random();
	for(int i = 0; i < h; i++) {
		for(int j= 0;j< w; j++) {
			data[i][j] = rand.nextInt(10);	
		}
	}	
}
public void print() {
	for(int i = 0; i < h; i++) {
		for(int j= 0;j< w; j++) {
			System.out.print(data[i][j]+ " ");
		}
		System.out.println("\n");
	}	
	System.out.println("==================================================================================================");
}
public matrix def() {
	String m;
	String n;
	
	int h, j;
	sc = new Scanner(System.in);
	System.out.println("Entrer la ligne  et le colonne du matrice:");
	
	try {
		m = sc.nextLine();
		n = sc.nextLine();
		h = Integer.parseInt(m);
		j = Integer.parseInt(n);
		matrix a = new matrix(h,j);
		return a;
		
	}
	catch(NumberFormatException e) {
		throw new Error("Invalide nombre de ligne ou de colonne");
	
	}
}
public void set(int i , int j , int val){
	this.data[i][j] = val;
}
public matrix multiply(matrix b) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, InterruptedException{
	matrix ret = new matrix(this.h, b.w);
	if(this.w != b.h ) {
		throw new Error("Impossible d'effectuer cette operation");
	}
	ArrayList<Multiplier> multipliers = new ArrayList<Multiplier>();
	for(int i = 0; i< h ; i++) {
		for(int j = 0; j < w; j++) {
			Multiplier a = main.multiplierclass.getConstructor().newInstance();
			a.execute(this, i, b , j , this.w);
			multipliers.add(a);
			
		}
	}
	for(int k = 0 ; k < multipliers.size();k++) {
		Multiplier multiplier = multipliers.get(k);
		if(multipliers.get(0) instanceof Th) {
			((Th) multiplier).join();
		}
		ret.set(multiplier.getLinNum(), multiplier.getColNum(), multiplier.get());
	}
	return ret;
}

}
