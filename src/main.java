import java.util.Scanner;

public class main {
	static Scanner sc;
	public static Class<? extends Multiplier> multiplierclass;
	
	public static void main(String[] args) throws Exception{
		String choix;
		/*matrix a = new matrix(0, 0);
		a = a.def();
		a.print();
		matrix b= new matrix(0,0);
		b = b.def();
		b.print();
		//matrix c = new matrix(0,0);
		//c = a.multiply(b);
		//c.print();
		 */
		System.out.println("Y or N for Multithreading");
		sc = new Scanner(System.in);
		choix= sc.nextLine();
		if(choix.charAt(0)=='Y' || choix.charAt(0)=='y') {
			
			multiplierclass = Th.class;
			matrix a = new matrix(0, 0);
			a = a.def();
			a.print();
			matrix b= new matrix(0,0);
			b = b.def();
			b.print();
			long t0 = System.nanoTime();
			matrix c = new matrix(0,0);
			c = a.multiply(b);
			c.print();
			long t1 = System.nanoTime();
			System.out.println("Elapsed time =" + ((t1 - t0))
			        + " nanoseconds");
			
		}
		else {
			
			multiplierclass= Seq.class;
			matrix a = new matrix(0, 0);
			a = a.def();
			a.print();
			matrix b= new matrix(0,0);
			b = b.def();
			b.print();
			long t0 = System.nanoTime();
			matrix c = new matrix(0,0);
			c = a.multiply(b);
			c.print();
			long t1 = System.nanoTime();
			System.out.println("Elapsed time =" + ((t1 - t0))
			        + " nanoseconds");
			
		}
		
		
	}
}