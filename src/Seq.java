
public class Seq implements Multiplier{
	private int res = 0;
	
	private matrix a,b;
	private int lin,col, len;
	


	@Override
	public void execute(matrix a, int lin, matrix b, int col, int len) {
		// TODO Auto-generated method stub
		this.lin = lin;
		this.col = col;
		this.len= len;
		this.a = a;
		this.b = b;
		
		for(int k = 0; k < len;k++) {
			res+= a.data[lin][k] * b.data[k][col];
		}
	}
	
	@Override
	public void end() throws InterruptedException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int get() {
		// TODO Auto-generated method stub
		return res;
	}

	@Override
	public int getLinNum() {
		// TODO Auto-generated method stub
		return lin;
	}

	@Override
	public int getColNum() {
		// TODO Auto-generated method stub
		return col;
	}
}
