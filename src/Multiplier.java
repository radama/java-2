
public interface Multiplier {
	public int getLinNum();
	public int getColNum();
	public void execute(matrix a, int h, matrix b, int w, int len);
	public void end() throws InterruptedException;
	public int get();
}
